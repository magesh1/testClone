Test Clone Project 
==================

This repo close files for Trainees to understand cloning



## Git global setup

```
 git config --global user.name "Magesh Mahadevan"
 git config --global user.email "magesh@purpleslate.in"
```

### Create a new repository

```
 git clone https://gitlab.com/magesh1/testClone.git
 cd testClone
 touch README.md
 git add README.md
 git commit -m "add README"
 git push -u origin master
```

### Existing folder
```
 cd existing_folder
 git init
 git remote add origin https://gitlab.com/magesh1/testClone.git
 git add .
 git commit -m "Initial commit"
 git push -u origin master
```

### Existing Git repository

```
 cd existing_repo
 git remote add origin https://gitlab.com/magesh1/testClone.git
 git push -u origin --all
 git push -u origin --tags
```